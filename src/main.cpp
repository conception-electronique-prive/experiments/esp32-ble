/*
    Based on Neil Kolban example for IDF:
   https://github.com/nkolban/esp32-snippets/blob/master/cpp_utils/tests/BLE%20Tests/SampleServer.cpp
    Ported to Arduino ESP32 by Evandro Copercini
    updates by chegewara
*/

#include <Arduino.h>
#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEUtils.h>
#include "state.hpp"

// See the following for generating UUIDs:
// https://www.uuidgenerator.net/

#define DISCOVERY_UUID "2c0c72f2-b8ba-4205-b541-fbe88c1c211b"
#define SERVICE_UUID "00000000-0000-1000-8000-00805f9b34fb"
#define CHARACTERISTIC_UUID "0000"

SonotacManager _manager;

class ServerCallbacks : public BLEServerCallbacks {
  // It seems that the advertising is automatically disabled when one device
  // connect We double-check just in case
  void onConnect(BLEServer* pServer) {
    Serial.println("Device connected");
    BLEDevice::stopAdvertising();
  }

  // Advertising doesn't automatically restart once all peers disconnect
  // We must start it ourselves once all peers leaves
  void onDisconnect(BLEServer* pServer) {
    Serial.println("Device disconnected");
    if (pServer->getConnectedCount() == 0) BLEDevice::startAdvertising();
  }
};

void setup() {
  Serial.begin(115200);
  Serial.println("Starting BLE work!");
  BLEServer* pServer;
  BLEService* pService;
  BLECharacteristic* pCharacteristic;

  // Start BLE stack, parameters is the name of the device
  BLEDevice::init("NeuroSpa");
  pServer = BLEDevice::createServer();
  pServer->setCallbacks(new ServerCallbacks());
  pService = pServer->createService(BLEUUID::fromString(SERVICE_UUID), 30U, 0U);
  _manager.init(pService);

  pService->start();
  // BLEAdvertising *pAdvertising = pServer->getAdvertising();  // this still is
  // working for backward compatibility
  BLEAdvertising* pAdvertising = BLEDevice::getAdvertising();
  pAdvertising->addServiceUUID(DISCOVERY_UUID);
  pAdvertising->setScanResponse(true);
  pAdvertising->setMinPreferred(
      0x06);  // functions that help with iPhone connections issue
  pAdvertising->setMinPreferred(0x12);
  BLEDevice::startAdvertising();
  Serial.println("Characteristic defined! Now you can read it in your phone!");
}

void loop() {
  _manager.run();
}