#pragma once

#include <cstdlib>

enum class CommandId {
    play,
    pause,
    stop,
    next,
    previous,
    select,
    setVolumes,
    getFavPagesCount,
    getFavPage,
    addFav,
    remFav,
    startFileTransfert,
    sendFileChunk,
    stopFileTransfert,
    getDeviceName,
    setDeviceName,
    getOwnerName,
    setOwnerName,
    getLicense,
    setLicense,
    getSerial,
    reset,
    max,
};

static constexpr size_t ST(CommandId id) {
    return static_cast<size_t>(id);
}