#pragma once

#include <BLEUUID.h>

#include <array>
#include <cstdlib>

enum class CharId {
  command,
  playing,
  start,
  offset,
  sound,
  vibration,
  program,
  extra,
  max,
};

static constexpr size_t ST(CharId id) { return static_cast<size_t>(id); }

static constexpr size_t FT(CharId id) { return 1 << static_cast<size_t>(id); }

static constexpr bool HF(CharId id, size_t flags) {
  return (FT(id) && flags) != 0;
}

extern std::array<BLEUUID, ST(CharId::max)> uuids;
