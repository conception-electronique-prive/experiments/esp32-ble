
#include <Esp.h>
#include <sys/time.h>

#include <ctime>

#include "state.hpp"

Date Date::now() {
  struct timeval tv;
  struct timezone tz = {0, 0};
  gettimeofday(&tv, &tz);
  std::tm* tm = std::gmtime(&(tv.tv_sec));
  Date d;
  d.year = tm->tm_year + 1900;
  d.month = tm->tm_mon;
  d.day = tm->tm_mday;
  d.hour = tm->tm_hour;
  d.minute = tm->tm_min;
  d.second = tm->tm_sec;
  return d;
}

void Date::setNow(Date d) {
  std::tm tm{};
  tm.tm_year = d.year - 1900;
  tm.tm_mon = d.month;
  tm.tm_mday = d.day;
  tm.tm_hour = d.hour;
  tm.tm_min = d.minute;
  tm.tm_sec = d.second;

  struct timeval tv = {std::mktime(&tm), 0};
  struct timezone tz = {0, 0};

  settimeofday(&tv, &tz);
}

uint8_t* Date::toArray(uint8_t* buf) {
  memcpy(buf, &year, sizeof(uint16_t));
  buf[2] = month;
  buf[3] = month;
  buf[4] = day;
  buf[5] = hour;
  buf[6] = minute;
  buf[7] = second;
  return buf;
}

Date Date::fromArray(uint8_t* buf) {
  Date d;
  d.year = *reinterpret_cast<uint16_t*>(buf);
  d.month = buf[2];
  d.day = buf[3];
  d.hour = buf[4];
  d.minute = buf[5];
  d.second = buf[6];
  return d;
}

char* Date::toString() {
  static char buf[200] = "";
  snprintf(buf, 200, "%04d/%02d/%02d %02d:%02d:%02d", year, month, day, hour,
           minute, second);
  return buf;
}
