#include <Arduino.h>

#include <functional>

#include "characteristics.hpp"
#include "state.hpp"

using namespace std::placeholders;

SonotacManager::SonotacManager() {
  handlers[ST(CommandId::play)] =
      std::bind(&SonotacManager::_handlePlay, this, _1);
  handlers[ST(CommandId::pause)] =
      std::bind(&SonotacManager::_handlePause, this, _1);
  handlers[ST(CommandId::stop)] =
      std::bind(&SonotacManager::_handleStop, this, _1);
  handlers[ST(CommandId::next)] =
      std::bind(&SonotacManager::_handleNext, this, _1);
  handlers[ST(CommandId::previous)] =
      std::bind(&SonotacManager::_handlePrevious, this, _1);
  handlers[ST(CommandId::select)] =
      std::bind(&SonotacManager::_handleSelect, this, _1);
  handlers[ST(CommandId::setVolumes)] =
      std::bind(&SonotacManager::_handleVolumes, this, _1);
  handlers[ST(CommandId::getFavPagesCount)] =
      std::bind(&SonotacManager::_handleGetFavPagesCount, this, _1);
  handlers[ST(CommandId::getFavPage)] =
      std::bind(&SonotacManager::_handleGetFavPage, this, _1);
  handlers[ST(CommandId::addFav)] =
      std::bind(&SonotacManager::_handleAddFav, this, _1);
  handlers[ST(CommandId::remFav)] =
      std::bind(&SonotacManager::_handleRemFav, this, _1);
  handlers[ST(CommandId::startFileTransfert)] =
      std::bind(&SonotacManager::_handleStartFileTransfert, this, _1);
  handlers[ST(CommandId::sendFileChunk)] =
      std::bind(&SonotacManager::_handleSendFileChunk, this, _1);
  handlers[ST(CommandId::stopFileTransfert)] =
      std::bind(&SonotacManager::_handleStopFileTransfert, this, _1);
  handlers[ST(CommandId::getDeviceName)] =
      std::bind(&SonotacManager::_handleGetDeviceName, this, _1);
  handlers[ST(CommandId::setDeviceName)] =
      std::bind(&SonotacManager::_handleSetDeviceName, this, _1);
  handlers[ST(CommandId::getOwnerName)] =
      std::bind(&SonotacManager::_handleGetOwnerName, this, _1);
  handlers[ST(CommandId::setOwnerName)] =
      std::bind(&SonotacManager::_handleSetOwnerName, this, _1);
  handlers[ST(CommandId::getLicense)] =
      std::bind(&SonotacManager::_handleGetLicense, this, _1);
  handlers[ST(CommandId::reset)] =
      std::bind(&SonotacManager::_handleReset, this, _1);

  Date::setNow(Date{2022, 3, 22, 14, 22, 00});
}

void SonotacManager::onWrite(BLECharacteristic* pCharacteristic) {
  recv(pCharacteristic->getData());
}

void SonotacManager::recv(uint8_t* data) {
  if (data[0] >= ST(CommandId::max)) {
    Serial.println("Invalid command, aborting");
  }
  Serial.printf("Command %d\r\n", data[0]);
  handlers[data[0]](data);
}

void SonotacManager::init(BLEService* service) {
  uint32_t read_property =
      BLECharacteristic::PROPERTY_READ | BLECharacteristic::PROPERTY_NOTIFY;

  uint32_t write_property = BLECharacteristic::PROPERTY_WRITE;

  characteristics[ST(CharId::command)] =
      service->createCharacteristic(uuids[ST(CharId::command)], write_property);
  characteristics[ST(CharId::command)]->setCallbacks(this);

  characteristics[ST(CharId::playing)] =
      service->createCharacteristic(uuids[ST(CharId::playing)], read_property);
  characteristics[ST(CharId::start)] =
      service->createCharacteristic(uuids[ST(CharId::start)], read_property);
  characteristics[ST(CharId::offset)] =
      service->createCharacteristic(uuids[ST(CharId::offset)], read_property);
  characteristics[ST(CharId::sound)] =
      service->createCharacteristic(uuids[ST(CharId::sound)], read_property);
  characteristics[ST(CharId::vibration)] = service->createCharacteristic(
      uuids[ST(CharId::vibration)], read_property);
  characteristics[ST(CharId::program)] =
      service->createCharacteristic(uuids[ST(CharId::program)], read_property);
  characteristics[ST(CharId::extra)] =
      service->createCharacteristic(uuids[ST(CharId::extra)], read_property);

  _pushState(0xff);
}

void SonotacManager::_pushState(size_t flags) {
  Serial.printf("%s\r\n\r\n", _state.toString());

  if (HF(CharId::playing, flags)) {
    uint16_t playing = _state.playing;
    characteristics[ST(CharId::playing)]->setValue(playing);
  }
  if (HF(CharId::start, flags)) {
    uint8_t bufDate[7];
    characteristics[ST(CharId::start)]->setValue(_state.start.toArray(bufDate),
                                                 7);
  }
  if (HF(CharId::offset, flags)) {
    characteristics[ST(CharId::offset)]->setValue(_state.offset);
  }
  if (HF(CharId::sound, flags)) {
    characteristics[ST(CharId::sound)]->setValue(_state.sound);
  }
  if (HF(CharId::vibration, flags)) {
    characteristics[ST(CharId::vibration)]->setValue(_state.vibration);
  }
  if (HF(CharId::program, flags)) {
    characteristics[ST(CharId::program)]->setValue(_state.program);
  }
}

void SonotacManager::_pushExtra(CommandId id, uint8_t* buf, size_t length) {
  buf[0] = static_cast<uint8_t>(id);
  characteristics[ST(CharId::extra)]->setValue(buf, length);
  for (int i = 0; i < length; ++i) {
    Serial.printf("%02x ", buf[i]);
  }
  Serial.printf("\r\n\r\n");
}

void SonotacManager::_handlePlay(uint8_t* data) {
  float offset;
  offset = *reinterpret_cast<float*>(data + 1);
  _state.playing = true;
  _state.start = Date::now();
  if (offset >= 0) {
    _state.offset = progress = offset;
  } else {
    _state.offset = progress;
  }
  _pushState(FT(CharId::playing) | FT(CharId::start) | FT(CharId::offset));
}

void SonotacManager::_handlePause(uint8_t* data) {
  _state.playing = false;
  _state.start = Date::now();
  _state.offset = ((float)progress / 1000.0);
  _pushState(FT(CharId::playing) | FT(CharId::start) | FT(CharId::offset));
}

void SonotacManager::_handleStop(uint8_t* data) {
  _state.playing = false;
  _state.start = Date::now();
  _state.offset = progress = 0.0f;
  _pushState(FT(CharId::playing) | FT(CharId::start) | FT(CharId::offset));
}

void SonotacManager::_handleNext(uint8_t* data) {
  _state.program++;
  if (_state.program > program_max) _state.program = program_min;

  if (_state.playing) {
    _state.start = Date::now();
    _state.offset = progress = 0.0f;
    _pushState(FT(CharId::start) | FT(CharId::offset) | FT(CharId::program));
  } else {
    _pushState(FT(CharId::program));
  }
}

void SonotacManager::_handlePrevious(uint8_t* data) {
  if (_state.program == program_min) {
    _state.program = program_max;
  } else {
    --_state.program;
  }

  if (_state.playing) {
    _state.start = Date::now();
    _state.offset = progress = 0.0f;
    _pushState(FT(CharId::start) | FT(CharId::offset) | FT(CharId::program));
  } else {
    _pushState(FT(CharId::program));
  }
}

void SonotacManager::_handleSelect(uint8_t* data) {
  _state.start = Date::now();
  _state.offset = progress = 0.0f;
  _state.program = data[1];
  _pushState(FT(CharId::start) | FT(CharId::offset) | FT(CharId::program));
}

void SonotacManager::_handleVolumes(uint8_t* data) {
  _state.sound = *reinterpret_cast<float*>(&data[1]);
  _state.vibration = *reinterpret_cast<float*>(&data[1 + sizeof(float)]);
  _pushState(FT(CharId::sound) | FT(CharId::vibration));
}

void SonotacManager::_handleGetFavPagesCount(uint8_t* data) {
  uint8_t buf[2] = {0, ceil(favorites.size() / favoritesPerPage)};
  _pushExtra(CommandId::getFavPagesCount, buf, 2);
}

void SonotacManager::_handleGetFavPage(uint8_t* data) {
  uint8_t pages = ceil((float)favorites.size() / (float)favoritesPerPage);
  if (data[1] > pages) return;
  uint8_t start = data[1] * favoritesPerPage;
  uint8_t buf[1 + favoritesPerPage];
  for (int i = 0; i < favoritesPerPage; ++i) {
    uint8_t index = i + start;
    if (i >= favorites.size())
      buf[i + 1] = -1;
    else
      buf[i + 1] = favorites[index];
  }
  _pushExtra(CommandId::getFavPage, buf, 1 + favoritesPerPage);
}

void SonotacManager::_handleAddFav(uint8_t* data) {
  for (auto fav : favorites) {
    if (fav == data[1]) return;
  }
  favorites.push_back(data[1]);
}

void SonotacManager::_handleRemFav(uint8_t* data) {
  for (int i = 0; i < favorites.size(); ++i) {
    if (favorites[i] == data[1]) {
      favorites.erase(favorites.begin() + i);
      return;
    }
  }
}

void SonotacManager::_handleGetDeviceName(uint8_t* data) {
  uint8_t buf[10];
  strncpy(reinterpret_cast<char*>(buf + 1), name, 9);
  _pushExtra(CommandId::getDeviceName, buf, 10);
}

void SonotacManager::_handleSetDeviceName(uint8_t* data) {
  strncpy(name, reinterpret_cast<char*>(data + 1), 8);
  uint8_t buf[10];
  strncpy(reinterpret_cast<char*>(buf + 1), name, 9);
  _pushExtra(CommandId::setDeviceName, buf, 10);
}

void SonotacManager::_handleGetLicense(uint8_t* data) {}

void SonotacManager::_handleSetLicense(uint8_t* data) {}

void SonotacManager::_handleGetSerial(uint8_t* data) {
  uint8_t serial[] = {"0012345678"};
  _pushExtra(CommandId::getSerial, serial, strlen((char*)serial));
}

void SonotacManager::_handleReset(uint8_t* data) {
  Date::setNow(Date::fromArray(data + 1));
}

void SonotacManager::run() {
  static constexpr uint32_t s = 250;
  if (_state.playing) progress += s;
  delay(s);
}