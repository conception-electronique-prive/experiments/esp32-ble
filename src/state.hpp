#pragma once

#include <Arduino.h>
#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEUtils.h>

#include <vector>
#include <array>
#include <cstdint>
#include <functional>

#include "characteristics.hpp"
#include "commands.hpp"

static constexpr uint32_t program_max = 10;
static constexpr uint32_t program_min = 0;

struct Date {
  uint16_t year;
  uint8_t month;
  uint8_t day;
  uint8_t hour;
  uint8_t minute;
  uint8_t second;

  static Date now();
  static void setNow(Date now);
  static Date fromArray(uint8_t* buf);
  uint8_t* toArray(uint8_t* buf);
  char* toString();
};

struct DeviceState {
  bool playing = 0;
  Date start = Date::now();
  float offset = 0;
  float sound = 0;
  float vibration = 0;
  uint32_t program = 0;

  char* toString() {
    static char buf[400] = "";
    snprintf(buf, 400,
             "Playing\t\t : %s\r\n"
             "Start\t\t : %s\r\n"
             "Offset\t\t : %0.0f\r\n"
             "Sound\t\t : %0.4f\r\n"
             "Vibra\t\t : %0.4f\r\n"
             "Program\t : %u\r\n",
             playing ? "True" : "False", start.toString(), offset, sound,
             vibration, program);
    return buf;
  }
};

class SonotacManager : public BLECharacteristicCallbacks {
  void onWrite(BLECharacteristic* pCharacteristic);

  DeviceState _state;
  uint32_t progress;
  char name[9] = "SonoTac";
  static constexpr uint8_t  favoritesPerPage = 3;
  std::vector<uint32_t> favorites;
  std::array<BLECharacteristic*, ST(CharId::max)> characteristics;
  std::array<std::function<void(uint8_t* data)>, ST(CommandId::max)> handlers;

  void _handlePlay(uint8_t* data);
  void _handlePause(uint8_t* data);
  void _handleStop(uint8_t* data);
  void _handleNext(uint8_t* data);
  void _handlePrevious(uint8_t* data);
  void _handleSelect(uint8_t* data);
  void _handleVolumes(uint8_t* data);
  void _handleGetFavPagesCount(uint8_t* data);
  void _handleGetFavPage(uint8_t* data);
  void _handleAddFav(uint8_t* data);
  void _handleRemFav(uint8_t* data);
  void _handleStartFileTransfert(uint8_t* data){}
  void _handleSendFileChunk(uint8_t* data) {}
  void _handleStopFileTransfert(uint8_t* data) {}
  void _handleGetDeviceName(uint8_t* data);
  void _handleSetDeviceName(uint8_t* data);
  void _handleGetOwnerName(uint8_t* data) {}
  void _handleSetOwnerName(uint8_t* data) {}
  void _handleGetLicense(uint8_t* data);
  void _handleSetLicense(uint8_t* data);
  void _handleGetSerial(uint8_t* data);
  void _handleReset(uint8_t* data);

  void _pushState(size_t values);
  void _pushExtra(CommandId id, uint8_t* buf, size_t length);

 public:
  SonotacManager();
  void recv(uint8_t* data);
  void init(BLEService* service);
  void run();
};