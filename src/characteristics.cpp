#include "characteristics.hpp"

std::array<BLEUUID, ST(CharId::max)> uuids = {
    BLEUUID::fromString("0000"),
    BLEUUID::fromString("0001"),
    BLEUUID::fromString("0002"),
    BLEUUID::fromString("0003"),
    BLEUUID::fromString("0004"),
    BLEUUID::fromString("0005"),
    BLEUUID::fromString("0006"),
    BLEUUID::fromString("0007"),
};
